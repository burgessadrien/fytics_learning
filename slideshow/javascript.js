var image = 0;
var run;

function shiftRight() {
    image++;
    if (image > 7) {
        image = 0;
    }
    project();

}

function shiftLeft() {
    image--;
    if (image < 0) {
        image = 7;
    }
    project();

}

function play() {
    run = setInterval(function(){
        image++;
        if (image > 7) {
            image = 0;
        }
        project();
    }, 5000);
}

function pause() {
    clearTimeout(run);
}

function project(){
        $("#projector").fadeTo(400, 0.2, function() {
            $("#projector").attr("src", "images/" + image + ".jpg").fadeTo(0, 0.2);
            $("#projector").fadeTo(500, 1);
        });
}

function slideCtl() {
    $(document).on("keydown", function(e){
        if(e.which == "37"){
            shiftLeft();
        }
        if(e.which == "39"){
            shiftRight();
        }
        if(e.which == "27"){
            location.href = 'http://www.huckmagazine.com/playlist-archive/escape-life-start-again-wanderlust/';
        }
        
    });
}


//functions to run when the page has loaded
window.onload = function() {
    slideCtl();
}


