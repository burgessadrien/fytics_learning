function remove(btnID){
    var btn = document.getElementById(btnID);
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
    
}

function edit(btnID) {
    var s = btnID.split('-');
    var col = "col" + btnID[5] + "-1";
    $td = $('#' + col);
    if($td.data('editing')) return;
    $td.data('editing', true);
    //storing old data
    $td.data("old", $td.text());
    var txt = $.trim($td.text());
    // make input
    var $input = $('<input type="text" class="editfield">');
    $input.val(txt);
    // clean td and add the input
    $td.empty();
    $td.append($input);
    $input.select();  
}

function save() {
    $("#reorderTable .editfield").each(function () {
        var val = $(this).val();
        var $td = $(this).closest('td');
        $td.empty();
        $td.text(val);
        $td.data('editing', false);
    });
}

function dontSave() {
    $("#reorderTable .editfield").each(function () {
        var old = $(this).closest('td').data('old');
        $td.empty();
        $td.text(old);
        $td.data('editing', false);
    });
}

function editCtl() {
    $(document).on("keydown", function(e){
        if(e.which == "13"){
            save();
        }
        if(e.which == "27"){
            dontSave();
        }
        
    });
}

function showHide(){
    $(".sideBtn").click(function() {
        $('.collapse.in').collapse('hide');
    });
}

function plusMinus() {
    $('#sideCol-1').on('hide.bs.collapse', function () {
        $('#sign1').removeClass("fa-minus");
        $('#sign1').addClass("fa-plus");
        $('#accbtn-1').removeClass("highlight");
      })
      $('#sideCol-1').on('show.bs.collapse', function () {
        $('#sign1').addClass("fa-minus");
        $('#sign1').removeClass("fa-plus");
        $('#accbtn-1').addClass("highlight");

      })
      $('#sideCol-2').on('hide.bs.collapse', function () {
        $('#sign2').removeClass("fa-minus");
        $('#sign2').addClass("fa-plus");
        $('#accbtn-2').removeClass("highlight");
      })
      $('#sideCol-2').on('show.bs.collapse', function () {
        $('#sign2').addClass("fa-minus");
        $('#sign2').removeClass("fa-plus");
        $('#accbtn-2').addClass("highlight");
      })
}

//functions to run when the page has loaded
window.onload = function() {
    editCtl();
    showHide();
    plusMinus();
}

